;(function () {

angular.module("filesBinder", [])
    .directive("files", filesBinderDirectiveFactory)

filesBinderDirectiveFactory.$inject = ["$q"];
function filesBinderDirectiveFactory($q) {
    return {
        scope: {
            accept: "@",
            maxFileSize: "<maxsize", //атрибут назван maxsize в целях единообразия с официальными атрибутами элемента input
        },

        restrict: "A",

        require: "ngModel",

        link: function (scope, element, attrs, ngModel) {
            var isBase64ing = !!(attrs["base64"] !== undefined && FileReader);
            var isMultiple = attrs["multiple"];
            var allowedTypes = [];
            var readers = [];

            function abortAllReaders() {
                readers.forEach(function (r) {
                    r.abort();
                });
                readers = [];
            }

            function readFileAsync(file) {
                return $q(function(resolve, reject) {
                    var fr = new FileReader();
                    fr.addEventListener("load", function () {
                        resolve({
                            name: file.name,
                            size : file.size ,
                            type  : file.type  ,
                            lastModified : file.lastModified,
                            dataUri: fr.result,
                        });
                    });
                    fr.addEventListener("error", function (e) {
                        reject(e);
                    });
                    fr.addEventListener("abort", function (e) {
                        reject(e);
                    });
                    fr.addEventListener("loadend", function (e) {
                        readers.splice(readers.indexOf(fr), 1);
                    });
                    readers.push(fr);
                    fr.readAsDataURL(file);
                });
            }

            function readFilesAsync(files) {
                return $q.all(Array.prototype.map.call(files, function (f) {
                    return readFileAsync(f);
                }));
            }

            if (attrs["accept"] !== undefined) {
                ngModel.$validators.allowedTypes = function (modelValue, viewValue) {
                    if (modelValue === undefined) return true;
                    if (!allowedTypes.length) return true;
                    var fileTypes;
                    if (isMultiple) {
                        fileTypes = Array.prototype.map.call(modelValue, function (file) {
                            return file.type;
                        });
                    }
                    else {
                        fileTypes = [modelValue.type];
                    }
                    return fileTypes.every(function (type) {
                        return allowedTypes.indexOf(type) >= 0;
                    });
                };
            }

            if (attrs["maxsize"] !== undefined) {
                ngModel.$validators.size = function (modelValue, viewValue) {
                    if (modelValue === undefined) return true;
                    var maxFileSize = +scope.maxFileSize;
                    if (isNaN(maxFileSize) || maxFileSize == 0) return true;
                    var fileSizes;
                    if (isMultiple) {
                        fileSizes = Array.prototype.map.call(modelValue, function (file) {
                            return file.size;
                        });
                    }
                    else {
                        fileSizes = [modelValue.size];
                    }
                    return fileSizes.every(function (size) {
                        return size <= maxFileSize;
                    });
                }
            }

            scope.$watch("accept", function (newValue, oldValue) {
                if (newValue === undefined) return; //единственный вызов для контрола без атрибута accept
                var allowedTypesStr = newValue.trim();
                allowedTypes = allowedTypesStr ? allowedTypesStr.split(/\s*,\s*/) : [];
                if (newValue === oldValue) return; //первый вызов, нет необходимости вызывать валидацию, это делается автоматически
                ngModel.$validate();
            });

            scope.$watch("maxFileSize", function (newValue, oldValue) {
                if (newValue === oldValue) return;
                ngModel.$validate();
            });

            element.on("change", function (event) {
                if (isBase64ing) {
                    abortAllReaders();
                    if (isMultiple) {
                        readFilesAsync(event.target.files).then(function (result) {
                            ngModel.$setViewValue(result);
                        }, function () {
                            ngModel.$setViewValue(undefined);
                        });
                    }
                    else {
                        readFileAsync(event.target.files[0]).then(function (result) {
                            ngModel.$setViewValue(result);
                        }, function () {
                            ngModel.$setViewValue(undefined);
                        });
                    }
                }
                else {
                    ngModel.$setViewValue(isMultiple ? event.target.files : event.target.files[0]);
                    scope.$apply();
                }
            });
        }
    };
}

})();
