angular
    .module("testbed", ["filesBinder"])
    .config(['$compileProvider', function ($compileProvider) {
        //чтобы Ангуляр не добавлял ссылкам на Data URI "unsafe:"
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|data):/);
    }])
    .controller("ExampleController", function ($scope) {
        $scope.showFile = function (file) {
            if (!file) return;
            var fileProps = {
                name: file.name,
                size: file.size,
                type: file.type,
            };
            if (file.dataUri) {
                fileProps.dataUri = file.dataUri.substr(0, 50) + "...";
            }
            return fileProps;
        };

        $scope.types = "image/png,text/html";
});
